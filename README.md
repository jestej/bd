# Tarea 2 INF-239 "Base de Datos"

## Criptomonedas: P-Coin II

________________________________________________________________________________________
### Integrantes:
- Alejandro Caceres  201930032-6
- Nicolas Capetillo  201930049-0  
- Vicente Tejos      201930017-2
________________________________________________________________________________________

# Primeros pasos
- [x] Colocar bases para trabajar en git.
- [x] Agregar a todos los miembros al git.
- [x] Revisar lo que solicita a desarrollar.

## Modelo de Datos
- [x] Distinguir los **Privilegios de Admin** (diferenciar entre usuario y admin).
- [x] Justificar la eleccion tomada , señalando ventajas y desventajas.

# Requisitos del Sistema

## Sesiones

- [x] Permitir que un Usuario pueda ingresar datos / Mantenerse en su sesion / Salir
- [x] Usuario puede Iniciar Sesion, Ingresando su correo y contraseña e una interfaz comun entregada por la plataforma.
- [x] Al ingresar sesion, el sitio determina si es Admin o User.
- [x] Usuario (Admin/User) tiene pestaña o interfaz donde puede ver su perfil (Informacion de cuenta).
- [x] Puede cerrar sesion desde cualquier vista de la plataforma.

## Interfaces
- [x] Pagina Principal 
- [x] Sign-up / Sign-in
- [x] CRUD (Solo el admin puede ingresar) 
     - [x] Create
     - [x] Read
     - [x] Update
     - [x] Delete
- [x] Perfil (cualquier usuario registrado la puede ver)
- [x] Wallet 
    - [x] Valor actual en USD.
    - [x] Conversion total.
    - [x] No mostrar monedas que no tenga el usuario.
    - [x] Vista solo para usuarios.
## Documentacion PDF
- [ ] Realizar archivo de documentacion (Informe) 
    - [x] Datos personales de cada alumno
    - [ ] Todos los supuestos y consideraciones que se tomaron.
    - [x] Como se manejo el modelo Sesiones Admin y usuario.
    - [ ] Dificultades con respecto a la implementacion.
    - [x] Tiempo que llevo en desarrollar la tarea completa.
