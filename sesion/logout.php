<?php
session_start();
$_SESSION['username'] = null;
$_SESSION['id'] = null;
session_destroy();
header("Location: ../index.html");
?>