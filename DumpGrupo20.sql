PGDMP     $                    y            postgres    12.6    12.6     ,           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            -           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            .           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            /           1262    13318    postgres    DATABASE     �   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Chile.1252' LC_CTYPE = 'Spanish_Chile.1252';
    DROP DATABASE postgres;
                postgres    false            0           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    2863                        3079    16384 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                   false            1           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                        false    1            �            1259    16393    cuenta_bancaria    TABLE     �   CREATE TABLE public.cuenta_bancaria (
    numero_cuenta integer NOT NULL,
    id_usuario integer NOT NULL,
    balance numeric NOT NULL
);
 #   DROP TABLE public.cuenta_bancaria;
       public         heap    postgres    false            �            1259    16399    moneda    TABLE     �   CREATE TABLE public.moneda (
    id integer NOT NULL,
    sigla character varying(10) NOT NULL,
    nombre character varying(80) NOT NULL
);
    DROP TABLE public.moneda;
       public         heap    postgres    false            �            1259    16402    pais    TABLE     g   CREATE TABLE public.pais (
    cod_pais integer NOT NULL,
    nombre character varying(45) NOT NULL
);
    DROP TABLE public.pais;
       public         heap    postgres    false            �            1259    16405    precio_moneda    TABLE     �   CREATE TABLE public.precio_moneda (
    id_moneda integer NOT NULL,
    fecha timestamp without time zone NOT NULL,
    valor numeric NOT NULL
);
 !   DROP TABLE public.precio_moneda;
       public         heap    postgres    false            �            1259    16411    usuario    TABLE     9  CREATE TABLE public.usuario (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50),
    correo character varying(50) NOT NULL,
    "contraseña" character varying(50) NOT NULL,
    pais integer NOT NULL,
    fecha_registro timestamp without time zone NOT NULL
);
    DROP TABLE public.usuario;
       public         heap    postgres    false            �            1259    16414    usuario_tiene_moneda    TABLE     �   CREATE TABLE public.usuario_tiene_moneda (
    id_usuario integer NOT NULL,
    id_moneda integer NOT NULL,
    balance numeric NOT NULL
);
 (   DROP TABLE public.usuario_tiene_moneda;
       public         heap    postgres    false            $          0    16393    cuenta_bancaria 
   TABLE DATA           M   COPY public.cuenta_bancaria (numero_cuenta, id_usuario, balance) FROM stdin;
    public          postgres    false    203   �"       %          0    16399    moneda 
   TABLE DATA           3   COPY public.moneda (id, sigla, nombre) FROM stdin;
    public          postgres    false    204   �#       &          0    16402    pais 
   TABLE DATA           0   COPY public.pais (cod_pais, nombre) FROM stdin;
    public          postgres    false    205   b$       '          0    16405    precio_moneda 
   TABLE DATA           @   COPY public.precio_moneda (id_moneda, fecha, valor) FROM stdin;
    public          postgres    false    206   �$       (          0    16411    usuario 
   TABLE DATA           d   COPY public.usuario (id, nombre, apellido, correo, "contraseña", pais, fecha_registro) FROM stdin;
    public          postgres    false    207   ,       )          0    16414    usuario_tiene_moneda 
   TABLE DATA           N   COPY public.usuario_tiene_moneda (id_usuario, id_moneda, balance) FROM stdin;
    public          postgres    false    208   0       �
           2606    16421 $   cuenta_bancaria cuenta_bancaria_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_pkey PRIMARY KEY (numero_cuenta);
 N   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_pkey;
       public            postgres    false    203            �
           2606    16423    moneda moneda_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.moneda
    ADD CONSTRAINT moneda_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.moneda DROP CONSTRAINT moneda_pkey;
       public            postgres    false    204            �
           2606    16425    pais pais_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (cod_pais);
 8   ALTER TABLE ONLY public.pais DROP CONSTRAINT pais_pkey;
       public            postgres    false    205            �
           2606    16427     precio_moneda precio_moneda_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.precio_moneda
    ADD CONSTRAINT precio_moneda_pkey PRIMARY KEY (id_moneda, fecha);
 J   ALTER TABLE ONLY public.precio_moneda DROP CONSTRAINT precio_moneda_pkey;
       public            postgres    false    206    206            �
           2606    16429    usuario usuario_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public            postgres    false    207            �
           2606    16431 .   usuario_tiene_moneda usuario_tiene_moneda_pkey 
   CONSTRAINT        ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_pkey PRIMARY KEY (id_usuario, id_moneda);
 X   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_pkey;
       public            postgres    false    208    208            �
           2606    16432 /   cuenta_bancaria cuenta_bancaria_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 Y   ALTER TABLE ONLY public.cuenta_bancaria DROP CONSTRAINT cuenta_bancaria_id_usuario_fkey;
       public          postgres    false    207    203    2718            �
           2606    16437 *   precio_moneda precio_moneda_id_moneda_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.precio_moneda
    ADD CONSTRAINT precio_moneda_id_moneda_fkey FOREIGN KEY (id_moneda) REFERENCES public.moneda(id);
 T   ALTER TABLE ONLY public.precio_moneda DROP CONSTRAINT precio_moneda_id_moneda_fkey;
       public          postgres    false    206    2712    204            �
           2606    16442    usuario usuario_pais_fkey    FK CONSTRAINT     z   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pais_fkey FOREIGN KEY (pais) REFERENCES public.pais(cod_pais);
 C   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pais_fkey;
       public          postgres    false    205    207    2714            �
           2606    16447 8   usuario_tiene_moneda usuario_tiene_moneda_id_moneda_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_moneda_fkey FOREIGN KEY (id_moneda) REFERENCES public.moneda(id);
 b   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_moneda_fkey;
       public          postgres    false    208    204    2712            �
           2606    16452 9   usuario_tiene_moneda usuario_tiene_moneda_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_tiene_moneda
    ADD CONSTRAINT usuario_tiene_moneda_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 c   ALTER TABLE ONLY public.usuario_tiene_moneda DROP CONSTRAINT usuario_tiene_moneda_id_usuario_fkey;
       public          postgres    false    208    207    2718            $   �   x���D1C�PC�l�u�����$����;9���r���N*��j.�~Ӓ�MX���u֕��(�>��bSi�:�=h�ds���T���H[&��p��KCN��R�6xA�SbP�G>)����T�`׏:Wt��QRw�;A�ϐu��Q��\�'���J�؋`�q+$9�|�AC0�p�d�-7$���m�������G��>"a�+{�cW}I"?��i�O��j�H�      %   �   x�-��
�@���>�O�[g��N`�F�b:#z}��ڝ��#�
��:8O���T�<���f6�^����B���{<
ma���fM��B�m�!2S�~�#�<C���e:!Q�?�:C��l�Q\�²��T�D��2�kED!�2�      &   t   x���1���p��Ox�. �DF��V�l�>'tC-4�F��`��U�p�����Y�!��KS�0�EBi���=�[S�coK���#.9O�Fq!&7��3��Y�6׏�"�i�#E      '   (  x�]X]�#�
{��*��D�o���צ�@F��H�SE�1��?m��m��y5�t\��aC�[��D��Υ������y�v	 ��%��{5ܲ/���iD�������8e����j��pw�y */Y�h�b��o����U=�s�Y���ٯ6/�W;yH��9/�z�OH�"{�`(�k?���<��߫���Ң���x[�E(�D,w΀����z᠖�E�Z �� 'Z��gb�ܢ�-���� y|P���x�l��c��\�T�,���]vp�{H�zɼ�]5\��O���������	������2����.RN�3+&W�P�R�.ᗈ��@�-$�]IYҞ'�\�L-���y2 �/R��0[��h�U��lK:���A��/y�P+��`�t�IpQpe_d��P#�z^DR����S:ؿD����g,G?�@�C���A��*���Ս����Ĳ���.t!/�S�B��~���d��ڞ���9��=\E�X���F��@�D��r\��.;-'�2Kz�� �FD��3�B�!$Tc���f��&җ� ��E͞����Ӡ��E���V\O[:5����Nn�)bٗ��3B��Ju�YF�^ w�͛(B� E�@�f�EZ8�f����9��D�.cYr�Ѓ,H诧���
1�\�Ŗ�S�gN8|{����B���zK.4�'缊fF�䰡d�E�|��trح<(Å��A9��!���8}���\":�Eh �����*��w+�y�S�����`�Re��r7�d,���XP�n���Q���`]+ukb��3g�SJŋ��r
�?5�2R���ȭ����RTz�tNN�ᮩH��&��\�s��B]v�h�h�rʦY�)Z��H��Y慩�]����V ����4�K�t.	a�mI�P�nToy�2����?�nd]!�j?��9��2�D�
9_�Ls#n�F�7�{�J�� �eLCM�=*�;D�0ҵ� �L�K4IK�֭ዐ�:�t7/R���4����$�y�OJֹ�Ao�H��$���q��$<�@�6N�cO�n��]Ȭ��k�=+�[�Ɋ#h�;ӟzy\�gN�����ٛ���E�0�̭M�-�n��EqQ�3%u8D
�T���8��&=�͕��΃��S�P��6��ӝn5�9�0�
���P\/w	�+��=l9u�ٌ����{ǋ�Ҙ���T����7�tIt���R/�S㔴[h���c�T�G犜��ي5�%�Eұ̳**O�_��-Mj+���A����}
�;��'��$�W`n��q�Y)�2M3q����\7����h�3в��q���~^�.�:���)|���e�]N�.��r���'[֏*��g�p����ren)D�/!J�me���!��K���?��Υg������B��,d��93}�N�ո$�G�>������M��|������.B��-�n��i�v=+���uU�9�|��`�w��

��
�s~���%��!�[�����>Y�ww��9ZO+�(�|s-֝+_@���Ve�V�(vR�=-}��ʨ�'+�;�Ö
�{����\�9O���&�R	�X��\g���i\��:�?�Ba�$���N��<�����1�;������}�ڟ�L�qJU�z|.f�R9ir����c��FZ��H'5+fxȫ���!so�JiΆ샛�������|��ϖ=�	j$��e�PY0���WN���֜����$�s���cK-6-J����m�Zl���������]\      (   �  x�U��n�8Eו��0AR�2��F�3�H��-�6;����	<_?E�Ih�0��u=n�Tv�v��nr�������u3�3U*.�mJe��Lo�P��_���{�����C�&x^����=���R��4�dQIm����S)�ڟ�<z����4O�_�<�����0,zy��eR��� ����6L}����vP/&�ß���i�?r�jQ���eQ�S~�Q@�B��͞}��v����u���5mK%W�������PvS������_���Glؕ�n��y�W��rVW��(PK��8ñsa��������e���Y�i[���
�Q�X&��)x:X���Y�bH&?2Ҳ�*�L�������O����.X��>9���0�ו15[�Zr�%Z��\�~��w��bL�b|3G��*��b��w4��;c(���kn����a��Xu]�ʱQ���"UL�'�b��cq?�R���!	1�IK�܈��-7\��S,vЉP�s�����Z`#��o�U)��UmE�r���~�i�9T���[xt��n%2FJ�/�y�7��d��5���]���s��n�����Z⏙h_}���n�|ÈIU�p��¤�r2�BIU3�ූ�	��|�E	O�l���k6�@�I���$X�(��5���z|��"���������<�.��Np\���"���Όɹ*u	�� �uLm��uc���a�����$��+����u�:��'��)���΍غGߝ츇��2��ugU�qV(#ͻ��b�,�d`�7;B��9��1����RY�\������t`���9G��u|�z�����8I*�E)L�Rh��`������0����&+�+%.Qu&dY��&K�!��XM�h�pكÐbxc�m�\���W���,*��z�_��XO�1J��d��ܰ�IQ�ۤ��X�x�������o�n�/���:�٥&N�}M�������\ �      )   �   x�%�ɍE1�3�2��2��1���X&-=(S�+�xN�LRO2q��ؑ�"K:�bу�&��M��;z�$�#~Q���G�TaLKF�aC�܋n��ػ�9o>���)y�O��d
�j�(�d�c�ԒE�������1� �![���)u��������%o�H�~f-��crn�#c;�>>����$2?�7�.k��P�_�=4     